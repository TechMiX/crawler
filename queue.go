package main

import "sync"

type QueueInterface interface {
	Enqueue(item interface{})
	Dequeue() interface{}
	Length() int
	Exist(item interface{}) bool
}

type InMemoryQueue struct {
	memory []interface{}
	index  map[interface{}]bool
	mutex  sync.Mutex
}

func (q *InMemoryQueue) Init() {
	q.index = make(map[interface{}]bool)
}

func (q *InMemoryQueue) Enqueue(item interface{}) {
	q.mutex.Lock()
	defer q.mutex.Unlock()

	q.memory = append(q.memory, item)
	q.index[SHA128(item.(string))] = true
}

func (q *InMemoryQueue) Dequeue() interface{} {
	q.mutex.Lock()
	defer q.mutex.Unlock()

	if len(q.memory) == 0 {
		return nil
	}

	d := q.memory[0]
	q.memory = q.memory[1:]

	delete(q.index, SHA128(d.(string)))

	return d
}

func (q *InMemoryQueue) Length() int {
	q.mutex.Lock()
	defer q.mutex.Unlock()

	return len(q.memory)
}

func (q *InMemoryQueue) Exist(item interface{}) bool {
	q.mutex.Lock()
	defer q.mutex.Unlock()

	_, ok := q.index[SHA128(item.(string))]
	return ok
}
