# Simple Web Crawler

This program is a simple web crawler that given a URL, it scraps the content, parse the HTML data and follows all the URLs defined as `A` tags.

All the data is stored in-memory, it binds itselft to `0.0.0.0:80` and you can command and query it via these REST API end-points:

## 1. Enqueue URL

Adds the URL to the crawling queue.

**URL** : `/enqueue`
**Method** : `POST`

### Success Response

**Code** : `200 OK`
**Content examples**
```json
{
    "id": "f33018c9b56bd42717ad99bbf45ff5f7c39b9ebd",
}
```

## 2. Get Info

To retreive info on a scraped URL via its identifier.

**URL** : `/info/{id}`
**Method** : `GET`

### Success Response

**Code** : `200 OK`
**Content examples**
```json
{
  "title": "Hello World",
  "date": "2002-10-02T15:00:00Z",
  "url": "https://example.com",
  "content": "<html><head><title>Hello World</title></head><body><h1>Hello World!!!</h1></body></html>", 
}
```

Run
-----
This program requires a `go` compiler. 
Navigate to the root of repository and then:
```
go build
```
This will generate an executable, run it like this:
```
./crawler
```

You can schedule a URL to be crawled with `curl`:
```
curl http://localhost/enqueue -F url=http://example.com
```