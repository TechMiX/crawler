package main

import (
	"bytes"
	"fmt"
	"log"
	"net/url"
	"time"

	"github.com/PuerkitoBio/goquery"
	"github.com/valyala/fasthttp"
)

type CrawledURL struct {
	Title   string `json:"title"`
	Date    string `json:"date"`
	URL     string `json:"url"`
	Content string `json:"content"`
}

type WebCrawler struct {
}

func (c *WebCrawler) Init() {
	for i := 0; i < 20; i++ {
		go c.Worker()
	}
}

func (c *WebCrawler) Worker() {
	for {
		for {
			URL := CQueue.Dequeue()
			if URL == nil {
				break
			}

			c.Scrap(URL.(string))
		}
		time.Sleep(1 * time.Second)
	}
}

func (c *WebCrawler) Scrap(URL string) {

	_, err := url.ParseRequestURI(URL)
	if err != nil {
		return
	}

	id := SHA128(URL)
	if CDatabase.Exist(id) {
		return
	}

	fmt.Println(id, URL)

	statusCode, body, err := fasthttp.Get(nil, URL)
	if err != nil || statusCode != fasthttp.StatusOK {
		log.Println("connection failed", URL, err, statusCode)
		return
	}

	doc, err := goquery.NewDocumentFromReader(bytes.NewReader(body))
	if err != nil {
		log.Println("parse failed", URL, err)
		return
	}

	newCrawledURL := CrawledURL{
		Title:   doc.Find("title").Text(),
		Date:    time.Now().Format(time.RFC3339),
		URL:     URL,
		Content: string(body),
	}
	CDatabase.Insert(&newCrawledURL)

	doc.Find("a").Each(func(i int, s *goquery.Selection) {
		newURL, exist := s.Attr("href")
		if exist && len(newURL) > 1 {

			// make relative URLs absolute!
			if string(newURL[0]) == "/" {
				if string(URL[len(URL)-1]) == "/" {
					newURL = URL[:len(URL)-2] + newURL
				} else {
					newURL = URL + newURL
				}
			}

			if !CQueue.Exist(newURL) {
				CQueue.Enqueue(newURL)
			}
		}
	})

}
