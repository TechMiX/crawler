package main

import (
	"bytes"
	"encoding/json"
	"log"

	"github.com/fasthttp/router"
	"github.com/valyala/fasthttp"
)

var CDatabase InMemoryCrawelerDatabase
var CQueue InMemoryQueue
var Crawler WebCrawler

func main() {

	CDatabase.Init()
	CQueue.Init()
	Crawler.Init()

	router := router.New()
	router.POST("/enqueue", EnqueueHandler)
	router.GET("/info/{id}", InfoHandler)

	err := fasthttp.ListenAndServe(":80", router.Handler)
	if err != nil {
		log.Fatal(err)
	}
}

func EnqueueHandler(ctx *fasthttp.RequestCtx) {
	url := string(ctx.FormValue("url"))
	if url == "" {
		ctx.Error("url parameter required", fasthttp.StatusBadRequest)
		return
	}

	CQueue.Enqueue(url)

	response := map[string]string{
		"id": SHA128(url),
	}
	responseJSON, _ := json.Marshal(response)

	ctx.SetContentType("application/json")
	ctx.Write(responseJSON)
}

func InfoHandler(ctx *fasthttp.RequestCtx) {
	id := ctx.UserValue("id").(string)

	if !CDatabase.Exist(id) {
		ctx.Error("Not found", fasthttp.StatusNotFound)
		return
	}

	result := new(bytes.Buffer)
	e := json.NewEncoder(result)
	e.SetEscapeHTML(false)
	e.Encode(CDatabase.Select(id))

	ctx.SetContentType("application/json")
	ctx.Write(result.Bytes())
}
