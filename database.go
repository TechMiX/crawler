package main

import "sync"

type CrawlerDatabaseInterface interface {
	Insert(item *CrawledURL) string
	Select(id string) *CrawledURL
	Exist(id string) bool
}

type InMemoryCrawelerDatabase struct {
	memory map[string]interface{}
	mutex  sync.Mutex
}

func (d *InMemoryCrawelerDatabase) Init() {
	d.memory = make(map[string]interface{})
}

func (d *InMemoryCrawelerDatabase) Insert(item *CrawledURL) string {
	id := SHA128(item.URL)

	d.mutex.Lock()
	defer d.mutex.Unlock()

	d.memory[id] = item
	return id
}

func (d *InMemoryCrawelerDatabase) Select(id string) *CrawledURL {
	d.mutex.Lock()
	defer d.mutex.Unlock()

	item, ok := d.memory[id].(*CrawledURL)
	if !ok {
		return nil
	}
	return item
}

func (d *InMemoryCrawelerDatabase) Exist(id string) bool {
	d.mutex.Lock()
	defer d.mutex.Unlock()

	_, ok := d.memory[id].(*CrawledURL)
	return ok
}

func (d *InMemoryCrawelerDatabase) Count() int {
	d.mutex.Lock()
	defer d.mutex.Unlock()

	return len(d.memory)
}
