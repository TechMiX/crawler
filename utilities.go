package main

import (
	"crypto/sha1"
	"encoding/hex"
)

func SHA128(input string) string {
	hasher := sha1.New()
	hasher.Write([]byte(input))
	result := hex.EncodeToString(hasher.Sum(nil))
	return result
}
